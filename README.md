
  

# README #

Utilizzando Node.js, è stato realizzato un servizio web per la lettura dei dati da un database SQLIte e che espone determinate Web API in JSON.

  
  

### Funzionamento ###

Il servizio va in esecuzione sul *localhost:3000* ed espone 3 diverse Web API in 3 diversi endpoint.

  
  

*  **Record List**

    \-ENDPOINT: GET localhost:3000/censimento/data?offset=0&count=10

    \-paginazione parametrica è possibile definire offset e count come query string 
    Restituisce la lista dei record denormalizzati.
---

*  **Statistics**

    \-ENDPOINT: POST localhost:3000/censimento/stats

    Restituisce un determinato set di statistiche aggregate filtrate in base ad alcuni parametri significativi passati nel body della richiesta che deve avere il seguente formato, dove "aggregationType" può assumere i valori: "age", "education_level_id", "occupation_id"
`

```json

  

{

"aggregationType":"age",

"aggregationValue": int

}

  

```

  
---  

*  **Download Data**

    \-ENDPOINT: GET localhost:3000/censimento/data/download

    permette il download in formato CSV di tutti i dati denormalizzati

  
  

  

### Esecuzione ###

  

  

* Installare i moduli utilizzati tramite il comando `npm i`

  

* Per mandare in esecuzione `node webApp.js`
var express=require('express');
var app=express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

const asyncHandler = require('express-async-handler')

var sqlite3 = require('sqlite3').verbose();
var sqlite = require('sqlite');
var fs = require('fs');
const { Parser } = require('json2csv');
const util = require('util');


async function getDataFromDb(limit, offset){
    const db = await sqlite.open({
      filename: 'exercise01.sqlite',
      driver: sqlite3.Database
    })
    var datas = []
    var tables = await db.all('SELECT name FROM sqlite_master WHERE type =\'table\' AND name NOT LIKE \'sqlite_%\';')
    for(i=0; i<tables.length -1; i++){
      datas.push(await db.all('SELECT * FROM ' + tables[i].name +''))
    }
    var result = [];
    var keyNames = [];
    const rowsCount = await db.each(
      'SELECT * FROM records LIMIT ' + limit + ' OFFSET ' + offset,
      [],
      (err, row) => {
        if (err) {
          throw err
        }
        count = 0;
        keyNames = Object.keys(row);
  
        for(i=0; i<keyNames.length; i++){
          if(keyNames[i].includes('_id')){
            for(k=0; k<datas[count].length; k++){
              if(datas[count][k].id == row[keyNames[i]]){
                row[keyNames[i]]=datas[count][k]
                break;
              }
            }
            count ++;
          }
        }
  
        result.push(row)
    
        // row = { col: 'other thing' }
      }
    )
    console.log(rowsCount)
    db.close();
    return result;
  }

async function getAllDataFromDb(){
    const db = await sqlite.open({
      filename: 'exercise01.sqlite',
      driver: sqlite3.Database
    })
    var datas = []
    var tables = await db.all('SELECT name FROM sqlite_master WHERE type =\'table\' AND name NOT LIKE \'sqlite_%\';')
    for(i=0; i<tables.length -1; i++){
      datas.push(await db.all('SELECT * FROM ' + tables[i].name +''))
    }
    var result = [];
    var keyNames = [];
    const rowsCount = await db.each(
      'SELECT * FROM records ',
      [],
      (err, row) => {
        if (err) {
          throw err
        }
        count = 0;
        keyNames = Object.keys(row);
  
        for(i=0; i<keyNames.length; i++){
          if(keyNames[i].includes('_id')){
            for(k=0; k<datas[count].length; k++){
              if(datas[count][k].id == row[keyNames[i]]){
                row[keyNames[i]]=datas[count][k]
                break;
              }
            }
            count ++;
          }
        }
  
        result.push(row)
    
        // row = { col: 'other thing' }
      }
    )
    console.log(rowsCount)
    db.close();

    const writeFile = util.promisify(fs.writeFile);
    var opts = { result };
    try {
      const parser = new Parser(opts);
      var csv = parser.parse(result);
      
        } catch (err) {
          console.error(err);
        }
    var namefile = 'data.csv'
    await writeFile(namefile, csv, function (err) {
        if (err)
            throw err;
        console.log('File' + namefile + ' is created successfully.');
    }); 
    return namefile;
  }

async function aggregateData(type, value){
    const db = await sqlite.open({
      filename: 'exercise01.sqlite',
      driver: sqlite3.Database
    })
    var datas = db.get('SELECT avg(capital_gain),sum(capital_gain), sum(capital_loss), avg(capital_loss), (SELECT count(over_50k) FROM records WHERE over_50k =1 AND ' + type + ' = ' + value +' ) AS over_50k_count, (SELECT count(over_50k)  FROM records WHERE over_50k =0 AND ' + type + ' = ' + value +' ) AS under_50k_count FROM records WHERE ' + type + ' = ' + value)
    db.close();
    return datas;
  }


//Download data
app.get('/censimento/data/download', asyncHandler(async(req, res) => {
    var name = await getAllDataFromDb()

    return res.download(name, name, function(err){
        if(err){
        }else{
            console.log('FILE CREATED');
            fs.unlink(name, function(){console.log('Deleted')});
            console.log('unlink done')
        }
    })
}));

//Stats
app.post('/censimento/stats', asyncHandler(async(req, res) => {

    var type = req.body.aggregationType
    var value = req.body.aggregationValue

    console.log(req.body)

    var result = await aggregateData(type, value)

    var response = {
        aggregationType: type,
        aggregationFilter: value,
        capital_gain_sum: result["avg(capital_gain)"],
        capital_gain_avg: result["sum(capital_gain)"],
        capital_loss_sum: result["sum(capital_loss)"],
        capital_loss_avg: result["avg(capital_loss)"],
        over_50k_count: result["over_50k_count"],
        under_50k_count: result["under_50k_count"]
    }


    return res.status(200).json(response);
}));

//Record List
app.get('/censimento/data', asyncHandler(async(req, res) => {

    var limit = parseInt(req.query.count)
    var offset = parseInt(req.query.offset)

    console.log('count: ' + limit + ', offset: ' + offset)

    
    var results = await getDataFromDb(limit,offset)


    return res.status(200).json(results);
}));


app.listen(3000,function() {
    console.log("Ready")
});

/*var datas = []

async function getDatas(){
    const db = await sqlite.open({
      filename: 'exercise01.sqlite',
      driver: sqlite3.Database
    })
    var datas = []
    var tables = await db.all('SELECT name FROM sqlite_master WHERE type =\'table\' AND name NOT LIKE \'sqlite_%\';')
    for(i=0; i<tables.length -1; i++){
      console.log(tables[i].name)
      datas.push(await db.all('SELECT * FROM ' + tables[i].name +''))
    }
    db.close();
    console.log(datas)
    return datas;
  }


async function accessdb2(datas, limit, offset){
    const db = await sqlite.open({
      filename: 'exercise01.sqlite',
      driver: sqlite3.Database
    })
    var result = [];
    var keyNames = [];
    var caount;
    const rowsCount = await db.each(
      'SELECT * FROM records LIMIT ' + limit + ' OFFSET ' + offset,
      [],
      (err, row) => {
        if (err) {
          throw err
        }
        count = 0;
        keyNames = Object.keys(row);
  
        for(i=0; i<keyNames.length; i++){
          if(keyNames[i].includes('_id')){
            for(k=0; k<datas[count].length; k++){
              if(datas[count][k].id == row[keyNames[i]]){
                row[keyNames[i]]=datas[count][k]
                break;
              }
            }
            count ++;
          }
        }
  
        result.push(row)
    
        // row = { col: 'other thing' }
      }
    )
    console.log(rowsCount)
    db.close();
    return result;
  } 

//var results = await accessdb2(datas, limit, offset);


app.listen(3000, asyncHandler(async(req, res) => {
    datas = await getDatas()
    console.log(datas) 
    console.log("Ready")
})); */